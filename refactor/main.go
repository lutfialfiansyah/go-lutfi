package main

import (
	"fmt"
	"strings"
)

func main() {
	strData := `(123)`
	_ = findFirstStringInBracket(strData)
}

func findFirstStringInBracket(str string) string {
	var res string
	if str != "" {
		indexFirstBracketFound := strings.Index(str, "(")
		runes := []rune(str)
		fmt.Println(runes, `runes`)
		indexClosingBracketFound := strings.Index(str, ")")
		fmt.Println(indexClosingBracketFound, `indexClosingBracketFound,`)
		if indexFirstBracketFound >= 0 && indexClosingBracketFound >= 0 {
			runes := []rune(str)
			return string(runes[indexFirstBracketFound+1])
		} else {
			return res
		}
	}
	return res
}
