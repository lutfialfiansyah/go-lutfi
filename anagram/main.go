package main

import (
	"fmt"
	"sort"
	"strings"
)

var (
	datas = []string{"kita", "atik", "tika", "aku", "kia", "makan", "kua"}
)

func checkAnagram(datas []string) {
	list := make(map[string][]string)
	var result [][]string

	for _, says := range datas {
		key := sortString(says)
		// MAPPING DATA AFTER SORT
		list[key] = append(list[key], says)
	}

	for _, words := range list {
		result = append(result, words)
	}
	fmt.Println(result)
}

func sortString(k string) string {
	convArray := strings.Split(k, "")
	sort.Strings(convArray)
	convString := strings.Join(convArray, "")

	return convString
}

func main() {
	checkAnagram(datas)
}
