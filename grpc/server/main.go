package main

import (
	"log"
	"net"

	pb "gitlab.com/lutfialfiansyah/go-lutfi/grpc/movie"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

// server is used to implement movie.MovieServer.
type server struct {
	savedMovies []*pb.MovieRequest
}

// GetMovies returns all customers by given filter
func (s *server) GetMovies(filter *pb.MovieFilter, stream pb.Movier_GetMoviesServer) error {
	// for _, movie := range s.savedMovies {
	// 	if filter.Keyword != "" {
	// 		if !strings.Contains(movie.Name, filter.Keyword) {
	// 			continue
	// 		}
	// 	}
	// 	if err := stream.Send(movie); err != nil {
	// 		return err
	// 	}
	// }
	return nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	// Creates a new gRPC server
	s := grpc.NewServer()
	pb.RegisterMovieServer(s, &server{})
	s.Serve(lis)
}
