package main

import (
	"io"
	"log"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	pb "gitlab.com/lutfialfiansyah/go-lutfi/grpc/movie"
)

const (
	address = "localhost:50051"
)

// getMovies calls the RPC method GetMovies of CustomerServer
func getMovies(client pb.MovieClient, filter *pb.MovieFilter) {
	// calling the streaming API
	stream, err := client.GetMovies(context.Background(), filter)
	if err != nil {
		log.Fatalf("Error on get customers: %v", err)
	}
	for {
		// Receiving the stream of data
		movie, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.GetMovies(_) = _, %v", client, err)
		}
		log.Printf("Movie: %v", movie)
	}
}
func main() {
	// Set up a connection to the gRPC server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	// Creates a new MovieClient
	client := pb.NewMovieConnection(conn)

	// Filter with an empty Keyword
	filter := &pb.MovieFilter{Keyword: ""}
	getMovies(client, filter)
}
